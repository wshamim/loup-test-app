import { combineReducers } from 'redux';

import articles from './articles';
import display from './display';

const rootReducer = combineReducers({
  articles,
  display,
});
export default rootReducer;
