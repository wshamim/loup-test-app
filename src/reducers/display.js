import { SET_DISPLAY_SIZE } from '../actions/types';

const display = (
  state = {
    displayType: 'xs',
    bPortrait: false,
  },
  action,
) => {
  switch (action.type) {
    case SET_DISPLAY_SIZE:
      return { ...state, bPortrait: action.bPortrait, displayType: action.displayType };

    default:
      return state;
  }
};

export default display;
