import { SELECT_ARTICLE } from '../actions/types';

const articles = (
  state = {
    items: [],
    selectedArticle: null,
  },
  action,
) => {
  switch (action.type) {
    case SELECT_ARTICLE:
      return { ...state, selectedArticle: action.selectedArticle };
    default:
      return state;
  }
};

export default articles;
