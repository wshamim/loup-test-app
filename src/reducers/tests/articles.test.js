import reducer from '../articles';
import { selectArticle } from '../../actions/ArticleActions';
import { SELECT_ARTICLE } from '../../actions/types';

const dummyArticle = { test: 1 };

describe('test article reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      items: [],
      selectedArticle: null,
    });
  });

  it('should handle SELECT_ARTICLE', () => {
    const reducerResult = reducer({}, selectArticle(dummyArticle));
    expect(reducerResult.selectedArticle).toEqual(dummyArticle);
  });
});
