import reducer from '../display';
import { setDisplaySize } from '../../actions/DisplayActions';
import { SET_DISPLAY_SIZE } from '../../actions/types';

const mockActionValue = { displayType: 'md', bPortrait: true };

describe('test display reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      displayType: 'xs',
      bPortrait: false,
    });
  });

  it('should handle SET_DISPLAY_SIZE', () => {
    const reducerResult = reducer(
      {},
      setDisplaySize(mockActionValue.bPortrait, mockActionValue.displayType),
    );
    expect(reducerResult).toEqual(mockActionValue);
  });
});
