/* eslint-disable react/no-array-index-key */
import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import AuthorList from '../../components/article/AuthorList';
import ArticleBlockText from '../../components/article/ArticleBlockText';
import ArticleBlockCard from '../../components/article/ArticleBlockCard';
import ImageDisplay from '../../components/common/ImageDisplay';
import RatingModal from '../../components/common/RatingModal';

type Props = {
  article: any,
};

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
    spacing: 3,
  },
}));

export default function ArticlePage(props: Props) {
  const classes = useStyles();

  const { article } = props;

  const blockItems = article.blocks.map((block, index) => {
    if (block.blockTypeId === 0) {
      return (
        <ArticleBlockText
          key={`bl-${index}`}
          content={block.content}
          variant="body1"
          align="left"
        />
      );
    }
    const contentBlocks = block.content.map((cardContent, indexContent) => (
      <ArticleBlockCard key={`bl-${index}-c-${indexContent}`} cardContent={cardContent} />
    ));
    return contentBlocks;
  });

  return (
    <div id="articlePage" className={classes.root}>
      <Grid container spacing={4} justify="center">
        <ImageDisplay imageList={article.imageList} altTitle={article.title} />

        <ArticleBlockText content={article.title} variant="h3" align="center" />

        <AuthorList authors={article.authors} />

        <ArticleBlockText content={article.summary} variant="subtitle1" align="left" />

        {blockItems}
      </Grid>
      <RatingModal />
    </div>
  );
}
