import React, { useEffect } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import { useSelector, useDispatch } from 'react-redux';

import ArticlePage from './ArticlePage';
import { setDisplaySize } from '../../actions/DisplayActions';
import { selectArticle } from '../../actions/ArticleActions';

import data from '../../data/article.json';

function Homepage() {
  const dispatch = useDispatch();
  const selectedArticle = useSelector((state) => state.articles.selectedArticle);

  useEffect(() => {
    if (!selectedArticle) {
      dispatch(selectArticle(data.result.article));
    }
  });

  useEffect(() => {
    handleResize();
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  });

  const handleResize = () => {
    const rootWidth = document.getElementById('root').clientWidth;
    const rootHeight = document.getElementById('root').clientHeight;
    const bPortrait = rootWidth < rootHeight;
    let displayType = 'xs';

    if (rootWidth < 576) {
      displayType = 'xs';
    } else if (rootWidth >= 576 && rootWidth < 768) {
      displayType = 'sm';
    } else if (rootWidth >= 768 && rootWidth < 992) {
      displayType = 'md';
    } else if (rootWidth >= 992 && rootWidth < 1200) {
      displayType = 'lg';
    } else if (rootWidth >= 1200) {
      displayType = 'xl';
    }
    dispatch(setDisplaySize(bPortrait, displayType));
  };

  return (
    <>
      <CssBaseline />
      <Container id="homePage" maxWidth="lg">
        {selectedArticle !== null && <ArticlePage article={selectedArticle} />}
      </Container>
    </>
  );
}

export default Homepage;
