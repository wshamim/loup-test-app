import { SET_DISPLAY_SIZE } from './types';

export function setDisplaySize(bPortrait, displayType) {
  return {
    type: SET_DISPLAY_SIZE,
    bPortrait,
    displayType,
  };
}
