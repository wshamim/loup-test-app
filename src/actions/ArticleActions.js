import { SELECT_ARTICLE } from './types';

export function selectArticle(selectedArticle) {
  return {
    type: SELECT_ARTICLE,
    selectedArticle,
  };
}
