export const getImageSrc = (imageList, curDisplay) => {
  let imgSrc = imageList.landscapemobile2x;

  if (curDisplay.displayType === 'xl') {
    imgSrc =
      imageList.landscapewidedesktop2x ||
      imageList.landscapewidemobile3x ||
      imageList.landscapewidedesktop1x ||
      imageList.landscapemobile2x;
  } else if (curDisplay.displayType === 'lg') {
    imgSrc =
      imageList.landscapewidedesktop1x ||
      imageList.landscapewidemobile2x ||
      imageList.landscapemobile2x;
  } else if (curDisplay.displayType === 'md') {
    imgSrc =
      imageList.landscapedesktop2x ||
      imageList.landscapemobile3x ||
      imageList.landscapetablet2x ||
      imageList.landscapemobile2x;
  }
  return imgSrc;
};
