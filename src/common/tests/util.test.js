import { getImageSrc } from '../util';

const mockData = {
  landscapewidedesktop2x: 'landscapewidedesktop2x',
  landscapewidedesktop1x: 'landscapewidedesktop1x',
  landscapedesktop2x: 'landscapedesktop2x',
  landscapemobile2x: 'landscapemobile2x',
};

describe('test getImageSrc util function', () => {
  it('should return the default value', () => {
    const utilResult1 = getImageSrc(mockData, { displayType: 'xs' });
    expect(utilResult1).toEqual('landscapemobile2x');
  });

  it('should return the xl value', () => {
    const utilResult1 = getImageSrc(mockData, { displayType: 'xl' });
    expect(utilResult1).toEqual('landscapewidedesktop2x');
  });

  it('should return the lg value', () => {
    const utilResult1 = getImageSrc(mockData, { displayType: 'lg' });
    expect(utilResult1).toEqual('landscapewidedesktop1x');
  });

  it('should return the md value', () => {
    const utilResult1 = getImageSrc(mockData, { displayType: 'md' });
    expect(utilResult1).toEqual('landscapedesktop2x');
  });
});
