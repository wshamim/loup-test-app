import React from 'react';
import AuthorList from '../AuthorList';
import renderer from 'react-test-renderer';

const mockAuthorList = [
  {
    name: 'mock author',
    imageList: {
      square1x: {
        url: 'https://test1.jpg',
      },
      square2x: {
        url: 'https://test2.jpg',
      },
      square3x: {
        url: 'https://test3.jpg',
      },
    },
  },
];

it('renders AuthorList correctly', () => {
  const tree = renderer.create(<AuthorList authors={mockAuthorList} />).toJSON();
  expect(tree).toMatchSnapshot();
});
