import React from 'react';
import ArticleBlockText from '../ArticleBlockText';
import renderer from 'react-test-renderer';

const mockContent1 = {
  content: 'mock content',
  variant: 'body1',
  align: 'center',
};

const mockContent2 = {
  content: 'mock content',
  variant: 'subtitle1',
  align: 'center',
};

const mockContent3 = {
  content: 'mock content',
  variant: 'h3',
  align: 'center',
};

it('renders ArticleBlockText body1 correctly', () => {
  const tree = renderer
    .create(
      <ArticleBlockText
        content={mockContent1.content}
        variant={mockContent1.variant}
        align={mockContent1.align}
      />,
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders ArticleBlockText subtitle1 correctly', () => {
  const tree = renderer
    .create(
      <ArticleBlockText
        content={mockContent2.content}
        variant={mockContent2.variant}
        align={mockContent2.align}
      />,
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders ArticleBlockText h3 correctly', () => {
  const tree = renderer
    .create(
      <ArticleBlockText
        content={mockContent3.content}
        variant={mockContent3.variant}
        align={mockContent3.align}
      />,
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
