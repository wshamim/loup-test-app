import React from 'react';
import ArticleBlockCard from '../ArticleBlockCard';
import renderer from 'react-test-renderer';

import { Provider } from 'react-redux';
import configureStore from '../../../store/configureStore';

const store = configureStore({
  display: {
    displayType: 'xl',
    bPortrait: false,
  },
});

const mockContent = {
  imageList: {
    landscapemobile2x: {
      url: 'http://test.image.com/test.png',
    },
  },
  imageReference: '',
  url: '/test',
  title: 'test tot;e',
  summary: 'test summary',
};

it('renders ImageDisplay correctly', () => {
  const tree = renderer
    .create(
      <Provider store={store}>
        <ArticleBlockCard cardContent={mockContent} />
      </Provider>,
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
