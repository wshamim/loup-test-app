/* eslint-disable react/no-array-index-key */
import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';

type Props = {
  authors: Array,
};

const useStyles = makeStyles((theme) => ({
  paper: {
    backgroundColor: theme.palette.background.default,
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    margin: theme.spacing(1),
    width: theme.spacing(20),
    height: theme.spacing(8),
    [theme.breakpoints.up('md')]: {
      width: theme.spacing(24),
      height: theme.spacing(12),
      marginBottom: theme.spacing(2),
    },
  },
  avatar: {
    width: theme.spacing(6),
    height: theme.spacing(6),
    [theme.breakpoints.up('md')]: {
      width: theme.spacing(8),
      height: theme.spacing(8),
    },
  },
  authorName: {
    color: theme.palette.text.primary,
    fontSize: '1rem',
    [theme.breakpoints.up('xl')]: {
      fontSize: '1.5rem',
    },
  },
}));

export default function AuthorList(props: Props) {
  const classes = useStyles();
  const { authors } = props;

  return (
    <Grid container justify="center">
      {authors.map((author, index) => (
        <Grid key={`au-${index}`} item>
          <Paper elevation={0} className={classes.paper}>
            <Avatar
              className={classes.avatar}
              alt={author.name}
              src={author.imageList.square2x.url}
            />
            <Typography className={classes.authorName} variant="subtitle2">
              {author.name}
            </Typography>
          </Paper>
        </Grid>
      ))}
    </Grid>
  );
}
