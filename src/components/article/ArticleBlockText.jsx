import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

type Props = {
  content: string,
  variant: string,
  align: string,
};

const useStyles = makeStyles((theme) => ({
  body1: {
    color: theme.palette.text.secondary,
    fontSize: '1.25rem',
    [theme.breakpoints.up('xl')]: {
      fontSize: '1.75rem',
    },
  },
  subtitle1: {
    color: theme.palette.text.secondary,
    fontStyle: 'italic',
    fontSize: '1.25rem',
    [theme.breakpoints.up('xl')]: {
      fontSize: '1.75rem',
    },
  },
  h3: {
    color: theme.palette.text.primary,
    fontSize: '2rem',
    [theme.breakpoints.up('xl')]: {
      fontSize: '2.5rem',
    },
  },
}));

export default function ArticleBlockText(props: Props) {
  const classes = useStyles();
  const { content, variant, align } = props;

  return (
    <Grid item xs={12} sm={12} md={12} lg={10} xl={10}>
      <Typography className={classes[variant]} variant={variant} align={align}>
        {content.replace(/(<([^>]+)>)/gi, '')}
      </Typography>
    </Grid>
  );
}
