import React from 'react';
import { useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';

import Typography from '@material-ui/core/Typography';

import { getImageSrc } from '../../common/util';

type Props = {
  cardContent: Object,
};

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: '100%',
  },
  cardHeader: {
    textAlign: 'center',
    padding: '10px',
    fontSize: '1.75rem',
    [theme.breakpoints.up('xl')]: {
      fontSize: '2.25rem',
    },
  },
  cardSummary: {
    color: theme.palette.text.secondary,
    fontSize: '1.125rem',
    [theme.breakpoints.up('xl')]: {
      fontSize: '1.625rem',
    },
  },
}));

export default function ArticleBlockCard(props: Props) {
  const classes = useStyles();
  const curDisplay = useSelector((state) => state.display);
  const { cardContent } = props;

  const imgSrc = getImageSrc(cardContent.imageList, curDisplay);

  const openRecipe = () => {
    window.open(`https://centr.com${cardContent.url}`, '_blank');
  };

  return (
    <Grid item xs={12} sm={12} md={12} lg={10} xl={10}>
      <Card className={classes.root}>
        <CardActionArea onClick={openRecipe}>
          <Typography className={classes.cardHeader} variant="h5" component="h5">
            {cardContent.title}
          </Typography>
          <CardMedia
            component="img"
            alt={cardContent.title}
            image={imgSrc.url}
            title={cardContent.title}
          />
          <CardContent>
            <Typography className={classes.cardSummary} variant="body1" component="p">
              {cardContent.summary}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  );
}
