import React from 'react';
import ImageDisplay from '../ImageDisplay';
import renderer from 'react-test-renderer';

import { Provider } from 'react-redux';
import configureStore from '../../../store/configureStore';

const store = configureStore({
  display: {
    displayType: 'xl',
    bPortrait: false,
  },
});

const mockImageList = {
  landscapemobile2x: {
    url: 'http://test.image.com/test.png',
  },
};

it('renders ImageDisplay correctly', () => {
  const tree = renderer
    .create(
      <Provider store={store}>
        <ImageDisplay imageList={mockImageList} altTitle="test title" />
      </Provider>,
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
