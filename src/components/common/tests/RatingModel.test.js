import React from 'react';
import RatingModel from '../RatingModal';
import renderer from 'react-test-renderer';

it('renders RatingModel correctly', () => {
  const tree = renderer.create(<RatingModel />).toJSON();
  expect(tree).toMatchSnapshot();
});
