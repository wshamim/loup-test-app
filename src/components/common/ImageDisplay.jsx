import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { useSelector } from 'react-redux';

import { getImageSrc } from '../../common/util';

type Props = {
  imageList: Array,
  altTitle: String,
};

const useStyles = makeStyles((theme) => ({
  image: {
    backgroundColor: theme.palette.background.paper,
    width: '100%',
  },
}));

export default function ImageDisplay(props: Props) {
  const classes = useStyles();
  const curDisplay = useSelector((state) => state.display);
  const { imageList, altTitle } = props;

  const imgSrc = getImageSrc(imageList, curDisplay);

  return (
    <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
      <img className={classes.image} src={imgSrc.url} alt={altTitle} />
    </Grid>
  );
}
