import React, { useState, useEffect } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Rating from '@material-ui/lab/Rating';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(1, 2),
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(2, 4),
    },
  },
  box: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    borderColor: 'transparent',
    justifyContent: 'space-evenly',
    width: '275px',
    height: '250px',
    maxHeight: '250px',
    minHeight: '250px',
    [theme.breakpoints.up('md')]: {
      width: '550px',
      height: '400px',
      maxHeight: '400px',
      minHeight: '400px',
    },
  },
  rating: {
    fontSize: '1.5rem',
    [theme.breakpoints.up('md')]: {
      fontSize: '2.5rem',
    },
  },
  button: {
    fontSize: '0.875rem',
    [theme.breakpoints.up('md')]: {
      fontSize: '1.5rem',
    },
  },
  body1: {
    fontSize: '0.875rem',
    [theme.breakpoints.up('md')]: {
      fontSize: '1.5rem',
    },
  },
  h6: {
    fontSize: '1rem',
    [theme.breakpoints.up('md')]: {
      fontSize: '2rem',
    },
  },
}));

export default function RatingModal() {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [shownOnce, setShownOnce] = useState(false);
  const [ratingValue, setRatingValue] = React.useState(3);
  const [ratingDone, setRatingDone] = useState(false);
  const [avgRating, setAvgRating] = useState(null);

  useEffect(() => {
    document.addEventListener('scroll', handleScroll);
    return () => {
      document.removeEventListener('scroll', handleScroll);
    };
  });
  const handleScroll = () => {
    const articlePageEl = document.getElementById('articlePage');
    const clRect = articlePageEl.getBoundingClientRect();
    if (clRect.height + clRect.top - 50 <= window.innerHeight) {
      document.removeEventListener('scroll', handleScroll);
      if (!shownOnce) {
        setOpen(true);
      }
    }
  };

  const handleClose = () => {
    setOpen(false);
    setShownOnce(true);
  };

  const ratingBody = (
    <div className={classes.paper}>
      <Box className={classes.box} component="div">
        <Typography className={classes.h6} align="center" variant="h6">
          WE&apos;D LOVE YOUR FEEDBACK...
        </Typography>
        <Typography className={classes.body1} align="center" variant="body1">
          How would you rate the content of this Article?
        </Typography>
        <Rating
          className={classes.rating}
          name="simple-controlled"
          value={ratingValue}
          onChange={(event, newValue) => {
            setRatingValue(newValue);
            setAvgRating(getAvgRating());
            setRatingDone(true);
          }}
        />

        <Button className={classes.button} variant="contained" onClick={handleClose}>
          Cancel
        </Button>
      </Box>
    </div>
  );

  const avgRatingBody = (
    <div className={classes.paper}>
      <Box className={classes.box} component="div">
        <Typography className={classes.h6} align="center" variant="h6">
          THANKS FOR YOUR FEEDBACK!
        </Typography>
        <Typography className={classes.body1} align="center" variant="body1">
          Average users have rated this article as
        </Typography>
        <Rating className={classes.rating} name="simple-readonly" value={avgRating} readOnly />
        <Button className={classes.button} variant="contained" onClick={handleClose}>
          Close
        </Button>
      </Box>
    </div>
  );

  const getAvgRating = () => {
    // TODO: replace with actual API call to get user rating.
    return Math.floor(Math.random() * 5) + 1;
  };

  const renderBody = ratingDone ? avgRatingBody : ratingBody;

  return (
    <Modal
      aria-labelledby="rating-modal-title"
      aria-describedby="rating-modal-description"
      className={classes.modal}
      open={open}
      onClose={handleClose}
    >
      {renderBody}
    </Modal>
  );
}
