# Front Coding Exercise

## Notes:
- I have used a custom boilerplate which I have created for myself to start a new project quickly.
- It can be checked from https://gitlab.com/wshamim/react-redux-simple-boilerplate
- I have a github account as well but I prefer to be discrete as that account is used by my current employer. This can be viewed at https://github.com/wshamim

## Dependencies

Dependencies expected to be installed on the system.

- Node.js `v13.7.0`

## How to build and run

1. Install dependencies using `yarn install`

2. Run the project as in production env using `yarn start`

3. The Project can be run Development mode using `yarn dev`

4. Test cases can run using `yarn test`

5. Hosted on Azure Storage Account for test purpose at https://louptestapp.z24.web.core.windows.net/

## Things to do (with more time):

- Customize styling as I have used Material UI library as a base theme
- Feedback/Rating Model is written hastily and it can be refactored to split code in more components for better testing
- Add some easing animation for Feedback dialog.
- Add Zoom functionality to Images while adding separate button for opening the URL
- Better detection and proper usage images based on the display size, orientation, and device
- Fix hot reloading in dev mode as it is broken for some reason :P